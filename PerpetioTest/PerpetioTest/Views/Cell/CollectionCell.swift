//
//  CollectionCell.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 26.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherTypeImgView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    
}
