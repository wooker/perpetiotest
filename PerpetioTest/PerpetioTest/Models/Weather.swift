//
//  Weather.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 20.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import Foundation
import ObjectMapper

class Weather: Mappable {
    
    var currentTime: String?
    var locationName: String?
    var weatherArray: [DayWeather]?
    
    init() {}
    required convenience init?(map: Map) { self.init() }
    
    func mapping(map: Map) {
        currentTime <- map["SiteRep.DV.dataDate"]
        locationName <- map["SiteRep.DV.Location.name"]
        weatherArray <- map["SiteRep.DV.Location.Period"]
    }
}


class DayWeather: Mappable {
    
    var day: String?
    var periodArray: [Period]?
    
    init() {}
    required convenience init?(map: Map) { self.init() }
    
    func mapping(map: Map) {
        day <- map["value"]
        periodArray <- map["Rep"]
    }
}


class Period: Mappable {
    
    var windDirection: String?
    var feelsLikeTemperature: String?
    var windGust: String?
    var screenRelativeHumidity: String?
    var precipitationProbability: String?
    var windSpeed: String?
    var temperature: String?
    var visibility: String?
    var weatherType: String?
    var maxUVIndex: String?
    
    init() {}
    required convenience init?(map: Map) { self.init() }
    
    func mapping(map: Map) {
        
        windDirection <- map["D"]
        feelsLikeTemperature <- map["F"]
        windGust <- map["G"]
        screenRelativeHumidity <- map["H"]
        precipitationProbability <- map["Pp"]
        windSpeed <- map["S"]
        temperature <- map["T"]
        visibility <- map["V"]
        weatherType <- map["W"]
        maxUVIndex <- map["U"]
    }
}
