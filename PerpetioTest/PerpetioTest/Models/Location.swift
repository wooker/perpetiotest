//
// Location.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 20.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import Foundation
import ObjectMapper

class Location: Mappable {
    
    var cities: [City]?

    init() {}
    required convenience init?(map: Map) { self.init() }
    
    func mapping(map: Map) {
        cities <- map["Locations.Location"]
    }
}

class City: Mappable {
    
    var name: String?
    var id: String?
    var unitaryAuthArea: String?
    var latitude: String?
    var longitude: String?
    var region: String?
    var elevation: String?
    
    init() {}
    required convenience init?(map: Map) { self.init() }
    
    func mapping(map: Map) {
        name <- map["name"]
        id <- map["id"]
        unitaryAuthArea <- map["unitaryAuthArea"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        region <- map["region"]
        elevation <- map["elevation"]
    }
}
