//
//  WeatherForecastVC.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 21.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import UIKit
import Foundation
import SVProgressHUD

let kCityForecastVC = "CityForecastVC"

class WeatherForecastVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var searchTextField: AutoCompleteTextField!
    @IBOutlet weak var weatherForecastButton: UIButton!
    @IBOutlet weak var weatherHistoryButton: UIButton!
    
    var citiesArray = [City]()
    var autocompleteCities = [City]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCities()
        setUpButtons()
        handleTextFieldInterfaces()
        configureTextField()
        searchTextField.delegate = self
    }
    
    @IBAction func weatherForecastButtonPressed(_ sender: UIButton) {
        guard let cityName = searchTextField.text else {return}
        for city in autocompleteCities {
            if city.name == cityName, let cityId = city.id {
                APIManager.shared.get3hourlyForecastFor(cityId: cityId, completion: { (weather, statusCode) in
                    if let weatherForecast = weather {
                        if let controller = self.storyboard?.instantiateViewController(withIdentifier: kCityForecastVC) as? CityForecastVC {
                            controller.weather = weatherForecast
                            self.navigationController?.show(controller, sender: self)
                        }
                    }
                })
                break
            }
        }
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (searchTextField.text?.isEmpty)! {
            weatherForecastButton.isUserInteractionEnabled = false
            weatherForecastButton.backgroundColor = UIColor.gray
        } else {
            weatherForecastButton.isUserInteractionEnabled = true
            weatherForecastButton.backgroundColor = UIColor.black
        }
    }
    
    
    // MARK: - Private Methods
    
    private func configureTextField(){
        weatherForecastButton.isUserInteractionEnabled = false
        weatherForecastButton.backgroundColor = UIColor.gray
        searchTextField.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        searchTextField.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 20.0)!
        searchTextField.autoCompleteCellHeight = 40.0
        searchTextField.maximumAutoCompleteCount = 100
        searchTextField.hidesWhenSelected = true
        searchTextField.hidesWhenEmpty = true
        searchTextField.enableAttributedText = true
        var attributes = [NSAttributedStringKey:Any]()
        attributes[NSAttributedStringKey.foregroundColor] = UIColor.black
        attributes[NSAttributedStringKey.font] = UIFont(name: "HelveticaNeue-Bold", size: 20.0)
        searchTextField.autoCompleteAttributes = attributes
    }
    
    private func handleTextFieldInterfaces(){
        
        searchTextField.onTextChange = {[weak self] text in
            
            if !text.isEmpty {
                var autoCompleteArray = [String]()

                if let cities = self?.citiesArray, !(cities.isEmpty) {
                    
                    for city in cities {
                        
                        if let cityName = city.name, cityName.hasPrefix(text) {
                            autoCompleteArray.append(cityName)
                            self?.autocompleteCities.append(city)
                        }
                    }
                    self?.searchTextField.autoCompleteStrings = autoCompleteArray
                }
            }
        }
    }
    
    private func setUpButtons() {
        weatherForecastButton.layer.cornerRadius = weatherForecastButton.frame.height / 2
        weatherHistoryButton.layer.cornerRadius = weatherHistoryButton.frame.height / 2
    }
    
    private func getCities() {
        
        SVProgressHUD.show()
        
        APIManager.shared.getAllCities { (cities, statusCode) in
            if let cities = cities {
                self.citiesArray = cities
                print(cities)
            }
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
        }
    }
}
