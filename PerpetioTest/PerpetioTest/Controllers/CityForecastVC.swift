//
//  CityForecastVC.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 26.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import UIKit

class CityForecastVC: UIViewController {

    var weather = Weather()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cityNameLabel.text = weather.locationName
    }
    
    fileprivate func setImageFrom(uvIndex: String) -> UIImage {
        
        if let index = Int(uvIndex) {
            
            switch index {
            case 0:
                return #imageLiteral(resourceName: "ClearNight")
            case 1:
                return #imageLiteral(resourceName: "Sun")
            case 2,3,5,6,7:
                return #imageLiteral(resourceName: "Clouds")
            case 8...27:
                return #imageLiteral(resourceName: "Rain")
            case 28...30:
                return #imageLiteral(resourceName: "Tunder")
            default:
              return #imageLiteral(resourceName: "Sun")
            }
        } else {
            return #imageLiteral(resourceName: "Sun")
        }
    }
    
}

extension CityForecastVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (weather.weatherArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        return cell
    }
}

extension CityForecastVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = weather.weatherArray?.first?.periodArray?.count {
            return count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as? CollectionCell else {fatalError("no collection cell")}
        if let temp = weather.weatherArray?.first?.periodArray![indexPath.item].temperature {
            collectionCell.temperatureLabel.text = temp
        }
        if let uvIndex = weather.weatherArray?.first?.periodArray![indexPath.item].maxUVIndex {
            collectionCell.weatherTypeImgView.image = setImageFrom(uvIndex: uvIndex)
        }
                
        
        return collectionCell
    }
    
    
}
