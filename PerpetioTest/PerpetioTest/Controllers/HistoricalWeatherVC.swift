//
//  HistoricalWeatherVC.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 21.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import UIKit
import SVProgressHUD

let kHistoryVC = "HistoryVC"

class HistoricalWeatherVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showHistoryButton: UIButton!
    
    private let meteoStationsList = ["Aberporth", "Armagh", "Ballypatrick Forest", "Bradford", "Braemar", "Camborne", "Cambridge NIAB", "Cardiff Bute Park", "Chivenor", "Cwmystwyth", "Dunstaffnage", "Durham", "Eastbourne", "Eskdalemuir", "Heathrow", "Hurn", "Lerwick", "Leuchars", "Lowestoft", "Manston", "Nairn", "Oxford", "Paisley", "Ringway", "Shawbury", "Sheffield", "Southampton", "Stornoway Airport", "Tiree", "Valley", "Waddington", "Whitby", "Yeovilton"]
    var selectedCity = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showHistoryButton.layer.cornerRadius = showHistoryButton.frame.height / 2
    }
    
    @IBAction func showHistoryButtonPressed(_ sender: UIButton) {
        if !selectedCity.isEmpty {
            
            guard let city = selectedCity.components(separatedBy: " ").first?.lowercased() else {return}
            SVProgressHUD.show()
            
                APIManager.shared.getHistoricalDataFor(city: city) { (weatherArray, statusCode) in
                    if let array = weatherArray {
                        if let controller = self.storyboard?.instantiateViewController(withIdentifier: kHistoryVC) as? HistoryVC {
                            controller.weatherArray = array.reversed()
                            controller.titleString = self.selectedCity
                            self.navigationController?.show(controller, sender: self)
                        }
                    }
                   
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
        }
    }
    
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meteoStationsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = meteoStationsList[indexPath.row]
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        selectedCity = meteoStationsList[indexPath.row]
    }
    
}
