//
//  HistoryVC.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 24.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import UIKit

let kCellIdentifier = "HistoryCell"

class HistoryVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var weatherArray = [[String: String]]()
    var titleString = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = titleString
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return weatherArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath) as? HistoryCell else {fatalError("HistoryCell is not exist")}
        let dict = weatherArray[indexPath.row]
        cell.yearLabel.text = dict["year"]
        cell.monthLabel.text = dict["month"]
        cell.maxTempLabel.text = dict["maxTemp"]
        cell.minTempLabel.text = dict["minTemp"]
        cell.airFrostLabel.text = dict["afDays"]
        cell.rainLabel.text = dict["rain"]
        cell.sunLabel.text = dict["sun"]
        return cell
    }

}
