//
//  APIManager.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 20.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireObjectMapper

class APIManager: NSObject {
    
    let baseURL = "http://datapoint.metoffice.gov.uk/public/data/"
    let apiKey = "df63eaeb-e0cb-4c31-852e-139faa41326e"
    
    static let shared = APIManager()
    
    func getAllCities(completion: @escaping([City]?, Int?) -> Void) {
        let url = "\(baseURL)val/wxfcs/all/json/sitelist?key=\(apiKey)"
        
        Alamofire.request(url).responseObject { (response: ((DataResponse<Location>))) in
            if let locations = response.result.value {
                completion(locations.cities, response.response?.statusCode)
            } else {
                completion(nil, response.response?.statusCode)
            }
        }
    }
    
    func get3hourlyForecastFor(cityId: String, completion: @escaping(Weather?, Int?) -> Void) {
        
        let url = "\(baseURL)val/wxfcs/all/json/\(cityId)?res=3hourly&key=\(apiKey)"
        
        Alamofire.request(url).responseObject { (response: ((DataResponse<Weather>))) in
            if let weather = response.result.value {
                completion(weather, response.response?.statusCode)
            } else {
                completion(nil, response.response?.statusCode)
            }
        }
    }
    
    func getDailyForecastFor(cityId: Int, completion: @escaping(Weather?, Int?) -> Void) {
        
    }
    
    func getHistoricalDataFor(city: String, completion: @escaping([[String: String]]?, Int?) -> Void) {
        
        let url = "https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/\(city)data.txt"
        Alamofire.request(url).responseString { (response) in
            
            if let txt = response.result.value {
                
                var weatherHistoryDict = [[String: String]]()
                var cuttedLines = txt.components(separatedBy: "\n") as [String]
                
                for _ in 0..<7 {
                    cuttedLines.removeFirst()
                }
                
                for line in cuttedLines {
                    
                    let trimString = (line.trimmingCharacters(in: .whitespacesAndNewlines)).condensedWhitespace
                    var dict = [String: String]()
                    
                    if trimString.components(separatedBy: " ").count == 7 {
                    for _ in 1..<line.count {
                        let value = trimString.components(separatedBy: " ")
                            dict["year"] = value[0]
                            dict["month"]  = value[1]
                            dict["maxTemp"] = value[2]
                            dict["minTemp"] = value[3]
                            dict["afDays"] = value[4]
                            dict["rain"] = value[5]
                            dict["sun"] = value[6]
                        }
                    }
                    if !dict.isEmpty {
                        weatherHistoryDict.append(dict)
                        dict.removeAll()
                    }
                }
                completion(weatherHistoryDict, response.response?.statusCode)
            } else {
                completion(nil, response.response?.statusCode)
            }
        }
    }
    
}
