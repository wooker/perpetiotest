//
//  StringExtension.swift
//  PerpetioTest
//
//  Created by Oleksandr Budz on 23.01.18.
//  Copyright © 2018 Oleksandr Budz. All rights reserved.
//

import Foundation

extension String {
    
    var condensedWhitespace: String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
}

